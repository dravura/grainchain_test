@extends('layout.app')

@section('content')
<div class="container">
    <div class="card-header">{{$text}}</div>
    <div class="card-header"><a href="{{route('home')}}">Regresar al inicio</a></div>
  <div class="row">
    <div class="col">Entrada original</div>
    <div class="col">Solucion</div>
    <div class="w-100"></div>
    <div class="col">@php
                        echo $tableOriginal
                   @endphp</div>
    <div class="col">@php
                        echo $tableSolution
                   @endphp</div>
  </div>
</div>
@endsection
