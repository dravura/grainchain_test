<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class dataController extends Controller
{
    public function procesa(Request $request)
    {

           //obtenemos el campo file definido en el formulario
           $file = $request->file('file');
           $contents = [];
           $lineas=0;
           $valid_matrix=[];
            foreach(file($file) as $line) 
            {              
                $line=explode(",", $line);
                $line=str_replace("\r\n","", $line);
                $columnas=0;
                $fila=[];
                foreach($line as $column)
                {
                    $fila[]=$column;
                    $columnas++;
                }
                $valid_matrix[]=$columnas;
                $contents[] = $fila;
                $lineas++;
            }
            $ext=$file->getClientOriginalExtension();
            if($ext!='txt')
            {
                echo "Extension invalida";
                exit();
            }
            $valid=$this->validColumns($valid_matrix);
            if(!$valid)
            {
                echo "El archivo esta incorrecto";
                exit();
            }
            $texto="la matriz es de ".$lineas." por ".$columnas;
            //pinto primero la entrada
            $tableOriginal=$this->printTable($contents);
            //echo $table;
            $datas=$contents;
            $contents = [];
            foreach($datas as $lines)
            {
                $fixLine=$this->checkLine($lines);
                $contents[] = $fixLine;
            }
            //primer limpieza filas
            $table=$this->printTable($contents);
            //echo $table;
            //Segun limpieza columnas
            //echo "<br><br>";
            $contents=$this->checkColumn($contents,$columnas,$lineas);
            $table=$this->printTable($contents);
            //echo $table;
            //echo "<br><br>";
            //corrijo tabla si faltan focos
            $contents=$this->checkMore($contents);
            $tableSolution=$this->printTable($contents);
            //echo $table;
            return view('procesa')
                    ->with('text',$texto)
                    ->with('tableOriginal',$tableOriginal)
                    ->with('tableSolution',$tableSolution);

    }

    public function printTable($datas)
    {
        $table="<table border=1>";
            foreach($datas as $lines)
            {
                $table.="<tr>";
                foreach($lines as $columns)
                {
                    $image="espacio.png";
                    if($columns==1)
                    {
                        $image="pared.png";
                    }
                    else if($columns==0)
                    {
                        $image="foco.png";
                    }
                    $table.="<td width=10px><img src='".url($image)."' width=30 heigth=30></td>";
                }
                $table.="</tr>";
            }
        $table.="</table>";

        return $table;
    }


    public function checkLine($lines)
    {
        $return_array=[];
        $exist=0;
        foreach($lines as $key=>$columns)
        {
            if($columns==0)
            {
                if($exist==0)
                {
                    $return_array[]=$columns;
                    $exist=1;
                }
                else
                {
                    $return_array[]=-1;
                }
                
            }
            else
            {
                $return_array[]=$columns;
                $exist=0;
                }
        }
        return $return_array;
    }

    public function checkColumn($columns,$number_colum,$number_lines)
    {
        for($i=0;$i<$number_colum-1;$i++)
        {
            $cols=array_column($columns, $i);
            $exist=0;
            $array_col=[];
            foreach($cols as $col)
            {
                if($col==0)
                {
                    if($exist==0)
                    {  
                        $array_col[]=$col;
                        $exist=1;
                    }
                    else
                    {
                        $array_col[]=-1;
                    }         
                }
                else
                {
                    $array_col[]=$col;
                    $exist=0;
                }
            }
            for($j=0;$j<$number_lines-1;$j++)
            {
                $columns[$j][$i]=$array_col[$j];
            }
            
        }
        return $columns;
    }

    public function checkMore($contents)
    {
        $pared=1;
        $espacio=-1;
        $foco=0;
        foreach($contents as $keyLine=>$line)
        {
            foreach($line as $keyColumn=>$column)
            {
                switch($column)
                {
                    case $pared:
                    break;
                    case $espacio:
                        //verifico hacia los lados y hacia arriba-abajo
                        //primero hacia los lados
                        $side=$this->sideBySide($line,$keyColumn);
                        $upDown=$this->upDown($contents,$keyColumn,$keyLine);
                        if($side && $upDown)
                        {
                            $contents[$keyLine][$keyColumn]=0;
                        }
                        //arriba abajo
                        //$upDown=$this->upDown($contents,$keyColumn);
                    break;
                    case $foco:
                    break;
                }
            }
        }
        return $contents;
    }

    public function sideBySide($line,$key)
    {
        //dd($line);
        $array_leftRight=[];
        //left

        for($l=$key;$l>0;$l--)
        {

            if($line[$l]==1)
            {
                break;
            }
            else
            {
                $array_leftRight[]=$line[$l];
            }

        }
        //right
        for($r=$key;$r<count($line);$r++)
        {
            if($line[$r]==1)
            {
                break;
            }
            else
            {
                $array_leftRight[]=$line[$r];
            }

        }
        if(count($array_leftRight)>0)
            {
                if(!in_array(0, $array_leftRight))
                {
                    //$line[$key]=1;
                    return true;
                }
            }
        return false;
    }

    public function upDown($contents,$keyColumn,$keyLine)
    {
        $line=array_column($contents,$keyColumn);
        $result=$this->sideBySide($line,$keyLine);

        return $result;
    }

    public function validColumns($array)
    {
        $val=0;
        foreach($array as $data)
        {
            if($val==0)//primer registro
            {
                $val=$data;
            }
            else
            {
                if($val==$data)
                {
                    $val=$data;
                }
                else
                {
                    return false;
                }
            }
        }
        return true;

    }

}
